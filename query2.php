
<?php
$activeQuery = 2;
include_once "obj/header.php";


include_once "obj/connect.php";

$sql = "SELECT accountId, COUNT(accountId) as count FROM (SELECT * FROM rental UNION SELECT * FROM previousrental) AS T1 GROUP BY accountId ORDER BY COUNT(accountId) DESC LIMIT 1";
$stmt= $link->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows

?>

<section>
    <center>
        <img class="imgHover" src="imgs/programming.gif" style="width:800px;height:400px;">
    </center>

    <div class="alert alert-primary text-center" role="alert">
        <?php echo $sql; ?>
    </div>
    <div class="container">
        <p class="text-center" style="width: 50%; margin: 0 auto">
            This query is all about finding the customer account who has rented the most movies from the store, both
            previously and currently.
            <br><br>
        </p>
    </div>
    <div class="container text-center" style="justify-content: center">
        <?php
        print "<pre>";
        print "<table border=1 style='margin: 0 auto'>";
        print "<tr><td>Account id </td><td> Count </td>";
        foreach ($result as $r)
        {
            print "\n";
            print "<tr><td>$r[accountId] </td><td> $r[count]  </td></tr>	";
        }
        print "</table>";
        print "</pre>";
        echo '<br><br><br><br>';
        ?>

    </div>
</section>

