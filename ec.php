<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/17/2020
 * Time: 2:28 PM
 */
include_once 'obj/connect.php';

$activeQuery = 4;
include_once "obj/header.php";



$stmt= $link->prepare("SELECT * FROM employee");
$stmt->execute();
$result = $stmt->get_result();
$employees = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows

?>

<section>
    <div class="container">
        <?php
        if (isset($_POST['submit'])){
            $stmt= $link->prepare("SELECT employee.ssn,lastName,firstName,hourlyRate FROM employee,hourlyemployee WHERE employee.ssn =? AND hourlyemployee.ssn = ?");
            $stmt->bind_param("ss", $_POST['employee'], $_POST['employee']);
            $stmt->execute();
            $result = $stmt->get_result();
            $employee = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows
            if ($result->num_rows === 0){
                echo '<h1>No employees found with that first and last name!</h1>';
                return;
            }
            $employee = $employee[0];
            echo '<div class="container">
            <ul>
                <li>4 Digits of SSN: '.explode("-", $employee['ssn'])[2].'</li>
                <li>First Name: '.$employee['firstName'].'</li>
                <li>Last Name: '.$employee['lastName'].'</li>
                <li>Hourly Rate: '.$employee['hourlyRate'].'</li>
            </ul>
        </div>';
            //TODO get the information for that specifc emplyoee
        }

        ?>
        <form class="user" id="forgotEmail" action="" method="post">
            <h2>Please select an employee to find out their information: </h2>
            <select id="employee" name="employee">
                <?php
                foreach ($employees as $e){
                    echo '<option value="'.$e['ssn'].'">'.$e['firstName'].' '.$e['lastName'].'</option>';
                }
                ?>
            </select>
            <div class="text-center">
                <input type="submit" name="submit" class="btn btn-success" value="Find information!">
            </div>
        </form>
    </div>
</section>
