<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/17/2020
 * Time: 1:43 PM
 */

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alexander Sigler - CS323</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))?>/../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))?>/../css/scrolling-nav.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php" <?php echo $activeQuery == 0 ? "active" : "" ?>>Assignement #4</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item <?php echo $activeQuery == 0 ? "active" : "" ?>">
                    <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
                </li>
                <li class="nav-item <?php echo $activeQuery == 1 ? "active" : "" ?>">
                    <a class="nav-link js-scroll-trigger" href="query1.php">Query 1</a>
                </li>
                <li class="nav-item <?php echo $activeQuery == 2 ? "active" : "" ?>">
                    <a class="nav-link js-scroll-trigger" href="query2.php">Query 2</a>
                </li>
                <li class="nav-item <?php echo $activeQuery == 3 ? "active" : "" ?>">
                    <a class="nav-link js-scroll-trigger" href="query3.php">Query 3</a>
                </li>
                <li class="nav-item <?php echo $activeQuery == 4 ? "active" : "" ?>">
                    <a class="nav-link js-scroll-trigger" href="ec.php">Extra Credit</a>
                </li>
            </ul>
        </div>
    </div>
</nav>