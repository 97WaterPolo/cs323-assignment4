<?php
/**
 * Created by PhpStorm.
 * User: Xander
 * Date: 4/17/2020
 * Time: 2:29 PM
 */
$activeQuery = 3;
include_once "obj/header.php";


include_once "obj/connect.php";

$sql = "SELECT t.ssn, e.lastName, e.firstName, sum((endTime-startTime)/100/100) AS hours, hourlyRate FROM timecard AS t LEFT OUTER JOIN hourlyemployee ON t.ssn = hourlyemployee.ssn LEFT OUTER JOIN employee as e ON t.ssn = e.ssn GROUP BY ssn";
$stmt= $link->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows

?>

<section>
    <center>
        <img class="imgHover" src="imgs/night.gif" style="width:800px;height:400px;">
    </center>
    <div class="alert alert-primary text-center" role="alert">
        <?php echo $sql; ?>
    </div>
    <div class="container">
        <p class="text-center" style="width: 50%; margin: 0 auto">
            This query is all about calculated the pay that is owed to each employee based upon their salary rate and
            the hours that they have logged in their time card! The values when displayed have been rounded!
            <br><br>
        </p>
    </div>
    <div class="container text-center" style="justify-content: center">
        <?php
        print "<pre>";
        print "<table border=1 style='margin: 0 auto'>";
        print "<tr><td>Account SSN </td><td>Name </td><td>Hourly Rate </td><td>Hours Worked </td><td> Pay </td>";
        foreach ($result as $r)
        {
            print "\n";
            print "<tr>";
            print "<td>".explode('-', $r['ssn'])[2]."</td>";
            print "<td>".$r['firstName'].' '.$r['lastName']."</td>";
            print "<td>".number_format(round($r['hourlyRate'], 3), 3)."</td>";
            print "<td>".number_format(round($r['hours'], 3), 3)."</td>";
            print "<td>$".round($r['hourlyRate']*$r['hours'], 2)."</td>";
            print "</tr>";
        }
        print "</table>";
        print "</pre>";
        echo '<br><br><br><br>';
        ?>

    </div>
</section>
