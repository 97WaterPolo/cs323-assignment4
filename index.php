<?php
$activeQuery = 0;
include_once "obj/header.php";

?>
<style>
    .imgHover{
        transition: transform 0.25s ease;
    }

    .imgHover:hover{
        transform: scale(1.5);
    }
</style>

<header class="bg-primary text-white">
    <div class="container text-center">
        <h1>Welcome to Alexander Sigler's CPSC 323 Assignment #4</h1>
        <p class="lead">A simple page to view the data in the Big Hit Video database!</p>
    </div>
</header>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <img class="imgHover" src="imgs/big.png" style="width:100%;height:100%;">
            </div>
            <div class="col-lg-3">
                <img class="imgHover" src="imgs/hit.jpg" style="width:100%;height:100%;">
            </div>
            <div class="col-lg-3">
                <img class="imgHover" src="imgs/video.jpg" style="width:100%;height:100%;">
            </div>
            <div class="col-lg-3">
                <img class="imgHover" src="imgs/database.jpg" style="width:100%;height:100%;">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 text-center">
                <h1>Big</h1>
            </div>
            <div class="col-lg-3 text-center">
                <h1>Hit</h1>
            </div>
            <div class="col-lg-3 text-center">
                <h1>Video</h1>
            </div>
            <div class="col-lg-3 text-center">
                <h1>Database</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2>About this assignment</h2>
                <p class="lead">
                    This page is all about demonstrating our skills in SQL by querying a given database and returning the
                    proper answers and values from it. This means that we are trying to execute the queries given in
                    our assignment and show that they are the same and work together!
                </p>
            </div>
        </div>
    </div>
</section>


<?php

include_once "obj/footer.php";

?>