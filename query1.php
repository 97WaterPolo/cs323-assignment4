
<?php
$activeQuery = 1;
include_once "obj/header.php";


include_once "obj/connect.php";

$sql = "SELECT accountId, videoId FROM( SELECT * FROM previousrental UNION SELECT * FROM rental) as combined;";
$stmt= $link->prepare($sql);
$stmt->execute();
$result = $stmt->get_result();
$result = $result->fetch_all(MYSQLI_ASSOC); // fetch an array of rows

?>

<section>
    <center>
        <img class="imgHover" src="imgs/tenor.gif" style="width:800px;height:400px;">
    </center>
    <div class="alert alert-primary text-center" role="alert">
        <?php echo $sql; ?>
    </div>
    <div class="container">
        <p class="text-center" style="width: 50%; margin: 0 auto">
            This query is all about listing all the information about the customer accounts and the videos that are currently
            rented and have been previously rented by that specific customer. This information is then outputted to a table
            for easy reading.
            <br><br>
        </p>
    </div>
    <div class="container text-center" style="justify-content: center">
        <?php
        print "<pre>";
        print "<table border=1 style='margin: 0 auto'>";
        print "<tr><td>Account id </td><td> Video id </td>";
        foreach ($result as $r)
        {
            print "\n";
            print "<tr><td>$r[accountId] </td><td> $r[videoId]  </td></tr>	";
        }
        print "</table>";
        print "</pre>";
        echo '<br><br><br><br>';
        ?>

    </div>
</section>

